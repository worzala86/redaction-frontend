import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Http} from '../../app.http';
import {ActivatedRoute, Router} from '@angular/router';

class Product {
    id: string;
    sku: string;
    name: string;
}

class Result {
    id: string;
    success: boolean;
}

@Component({
    selector: 'app-admin-product-edit',
    templateUrl: './admin-product-edit.component.html',
    styleUrls: ['./admin-product-edit.component.css']
})
export class AdminProductEditComponent implements OnInit {
    productForm;

    constructor(private fb: FormBuilder, private http: Http, private router: Router, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.productForm = this.fb.group({
            sku: ['', [Validators.required]],
            name: ['', [Validators.required]],
        });
        const code = this.route.snapshot.paramMap.get('id');
        if (code) {
            const resultObservable = this.http.get<Product>('products/' + code);
            resultObservable.subscribe(productResult => {
                this.productForm = this.fb.group({
                    sku: [productResult.sku, [Validators.required]],
                    name: [productResult.name, [Validators.required]],
                });
            });
        }
    }

    save() {
        if (this.productForm.status === 'INVALID') {
            return false;
        }
        const data = {
            sku: this.productForm.value.sku,
            name: this.productForm.value.name,
        };
        const code = this.route.snapshot.paramMap.get('id');
        if (code) {
            const resultObservable = this.http.put<Result>('products/' + code, data);
            resultObservable.subscribe(registerResult => {
                if (registerResult.success) {
                    this.router.navigate(['/admin/produkty']);
                } /*else if (registerResult.error === 'AccountExistsException') {
                this.modalService.open(AccountExistsModalComponent);
            }*/
            });
        } else {
            const resultObservable = this.http.post<Result>('products', data);
            resultObservable.subscribe(registerResult => {
                if (registerResult.id) {
                    this.router.navigate(['/admin/produkty']);
                } /*else if (registerResult.error === 'AccountExistsException') {
                this.modalService.open(AccountExistsModalComponent);
            }*/
            });
        }
        return false;
    }

    onFileChanged(event) {
        const selectedFile = event.target.files[0];
        const data = new FormData();
        data.append('file', selectedFile, selectedFile.name);
        const resultObservable = this.http.post<Result>('files', data);
        resultObservable.subscribe(registerResult => {
            /*if (registerResult.id) {
                this.router.navigate(['/admin/produkty']);
            } *//*else if (registerResult.error === 'AccountExistsException') {
                this.modalService.open(AccountExistsModalComponent);
            }*/
        });
    }

}
