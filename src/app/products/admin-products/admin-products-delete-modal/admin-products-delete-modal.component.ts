import { Component, OnInit } from '@angular/core';
import {MzBaseModal} from 'ngx-materialize';

@Component({
  selector: 'app-admin-products-delete-modal',
  templateUrl: './admin-products-delete-modal.component.html',
  styleUrls: ['./admin-products-delete-modal.component.css']
})
export class AdminProductsDeleteModalComponent extends MzBaseModal {
}
