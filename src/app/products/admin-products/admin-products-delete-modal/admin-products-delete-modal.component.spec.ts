import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProductsDeleteModalComponent } from './admin-products-delete-modal.component';

describe('AdminProductsDeleteModalComponent', () => {
  let component: AdminProductsDeleteModalComponent;
  let fixture: ComponentFixture<AdminProductsDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProductsDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProductsDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
