import {Component, OnInit} from '@angular/core';
import {Http} from '../../app.http';
import {MzModalService} from 'ngx-materialize';
import {AdminProductsDeleteModalComponent} from './admin-products-delete-modal/admin-products-delete-modal.component';

class Product {
    id: string;
    sku: string;
    name: string;
}

class Products {
    products: Product[];
    totalCount: number;
}

class DeleteResult {
    success: boolean;
}

@Component({
    selector: 'app-admin-products',
    templateUrl: './admin-products.component.html',
    styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit {
    products: Product[];

    constructor(private http: Http, private modalService: MzModalService) {
    }

    ngOnInit() {
        const ProductsComponent = this;
        const resultObservable = this.http.get<Products>('products');
        resultObservable.subscribe(productsResult => {
            ProductsComponent.products = productsResult.products;
        });
    }

    delete(product) {
        const result = this.modalService.open(AdminProductsDeleteModalComponent);
        console.log(result);
        // const resultObservable = this.http.delete<DeleteResult>('products/' + product.id);
    }
}
