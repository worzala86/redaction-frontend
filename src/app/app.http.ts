import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class Http {
    constructor(private http: HttpClient) {}

    get<Result>(url: string) {
        return this.http.get<Result>('http://redaction-backend.localhost/api/' + url, this.getOptions());
    }

    post<Result>(url: string, data: any) {
        return this.http.post<Result>('http://redaction-backend.localhost/api/' + url, data, this.getOptions());
    }

    put<Result>(url: string, data: any) {
        return this.http.put<Result>('http://redaction-backend.localhost/api/' + url, data, this.getOptions());
    }

    delete<Result>(url: string) {
        return this.http.delete<Result>('http://redaction-backend.localhost/api/' + url, this.getOptions());
    }

    private getOptions() {
        const httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
        });
        return {
            headers: httpHeaders,
            withCredentials: true
        };
    }
}
