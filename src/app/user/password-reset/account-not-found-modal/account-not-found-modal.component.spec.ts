import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNotFoundModalComponent } from './account-not-found-modal.component';

describe('AccountNotFoundModalComponent', () => {
  let component: AccountNotFoundModalComponent;
  let fixture: ComponentFixture<AccountNotFoundModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountNotFoundModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountNotFoundModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
