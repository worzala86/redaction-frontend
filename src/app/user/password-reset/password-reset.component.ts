import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Http} from '../../app.http';
import {Router} from '@angular/router';
import {MzModalService} from 'ngx-materialize';
import {AccountNotFoundModalComponent} from './account-not-found-modal/account-not-found-modal.component';

class Result {
    success: boolean;
    error: string;
}

@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {
    resetPasswordForm;
    names = {email: ''};

    constructor(private fb: FormBuilder, private http: Http, private router: Router, private modalService: MzModalService) {
        this.names = {
            email: 'nameEmail' + new Date().getTime(),
        };
    }

    ngOnInit() {
        this.resetPasswordForm = this.fb.group({
            email: ['', [Validators.pattern(/[a-z0-9/.]+@[a-z0-9/.]+.[a-z0-9]+/)]],
        });
    }

    resetPassword() {
        if (this.resetPasswordForm.status === 'INVALID') {
            return false;
        }
        const data = {
            mail: this.resetPasswordForm.value.email,
        };
        const resultObservable = this.http.post<Result>('reset-password', data);
        resultObservable.subscribe(resetPasswordResult => {
            if (resetPasswordResult.success) {
                this.router.navigate(['/zresetowano-haslo']);
            } else if (resetPasswordResult.error === 'UserNotFoundException') {
                this.modalService.open(AccountNotFoundModalComponent);
            }
        });
        return false;
    }

}
