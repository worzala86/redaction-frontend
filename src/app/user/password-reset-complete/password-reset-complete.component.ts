import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password-reset-complete',
  templateUrl: './password-reset-complete.component.html',
  styleUrls: ['./password-reset-complete.component.css']
})
export class PasswordResetCompleteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
