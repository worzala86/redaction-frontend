import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncorrectPasswordModalComponent } from './incorrect-password-modal.component';

describe('IncorrectPasswordModalComponent', () => {
  let component: IncorrectPasswordModalComponent;
  let fixture: ComponentFixture<IncorrectPasswordModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncorrectPasswordModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncorrectPasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
