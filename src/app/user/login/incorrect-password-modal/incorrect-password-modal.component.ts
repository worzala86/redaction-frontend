import { Component, OnInit } from '@angular/core';
import {MzBaseModal} from 'ngx-materialize';

@Component({
  selector: 'app-incorrect-password-modal',
  templateUrl: './incorrect-password-modal.component.html',
  styleUrls: ['./incorrect-password-modal.component.css']
})
export class IncorrectPasswordModalComponent extends MzBaseModal {
}
