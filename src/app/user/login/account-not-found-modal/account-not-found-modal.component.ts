import { Component, OnInit } from '@angular/core';
import {MzBaseModal} from 'ngx-materialize';

@Component({
  selector: 'app-account-not-found-modal',
  templateUrl: './account-not-found-modal.component.html',
  styleUrls: ['./account-not-found-modal.component.css']
})
export class AccountNotFoundModalComponent extends MzBaseModal {
}
