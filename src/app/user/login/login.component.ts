import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Http} from '../../app.http';
import {sha512} from 'js-sha512';
import {Router} from '@angular/router';
import {MzModalService} from 'ngx-materialize';
import {AccountNotFoundModalComponent} from './account-not-found-modal/account-not-found-modal.component';
import {IncorrectPasswordModalComponent} from './incorrect-password-modal/incorrect-password-modal.component';
import { Events } from 'ionic-angular';

class Result {
    redirect: string;
    error: string;
}

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    loginForm;
    errorMessages: object;
    names = {email: '', password: ''};

    constructor(private fb: FormBuilder, private http: Http, private router: Router, private modalService: MzModalService,
                private events: Events) {
        this.names = {
            email: 'nameEmail' + new Date().getTime(),
            password: 'password' + new Date().getTime(),
        };
    }

    checkPassword(form) {
        if (!form._parent) {
            return {passwordToShort: true};
        }
        const controls = form._parent.controls;
        const lengthAccepted = controls.password.value.toString().length >= 8;
        if (lengthAccepted) {
            return true;
        }
        return {passwordToShort: true};
    }

    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.pattern(/[a-z0-9/.]+@[a-z0-9/.]+.[a-z0-9]+/)]],
            password: ['', this.checkPassword],
        });

        this.errorMessages = {
            password: {
                passwordToShort: `Wpisane hasło musi mieć minimum 8 znaków`,
            },
        };
    }

    login() {
        if (this.loginForm.status === 'INVALID') {
            return false;
        }
        const data = {
            mail: this.loginForm.value.email,
            password: sha512(this.loginForm.value.password),
        };
        const resultObservable = this.http.post<Result>('login', data);
        resultObservable.subscribe(loginResult => {
            if (loginResult.redirect) {
                // this.router.navigate(['/' + loginResult.redirect]);
                this.events.publish('user:reload');
            } else if (loginResult.error === 'UserNotFoundException') {
                this.modalService.open(AccountNotFoundModalComponent);
            } else if (loginResult.error === 'IncorrectPasswordException') {
                this.modalService.open(IncorrectPasswordModalComponent);
            }
        });
        return false;
    }

}
