import { Component } from '@angular/core';
import {MzBaseModal} from 'ngx-materialize';

@Component({
  selector: 'app-wrong-code-modal',
  templateUrl: './wrong-code-modal.component.html',
  styleUrls: ['./wrong-code-modal.component.css']
})
export class WrongCodeModalComponent extends MzBaseModal {
}
