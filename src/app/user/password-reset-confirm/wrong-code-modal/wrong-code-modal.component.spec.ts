import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongCodeModalComponent } from './wrong-code-modal.component';

describe('WrongCodeModalComponent', () => {
  let component: WrongCodeModalComponent;
  let fixture: ComponentFixture<WrongCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrongCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrongCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
