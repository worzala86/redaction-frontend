import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Http} from '../../app.http';
import {ActivatedRoute, Router} from '@angular/router';
import {MzModalService} from 'ngx-materialize';
import {sha512} from 'js-sha512';
import {WrongCodeModalComponent} from './wrong-code-modal/wrong-code-modal.component';

class Result {
    success: boolean;
    error: string;
}

@Component({
    selector: 'app-password-reset-confirm',
    templateUrl: './password-reset-confirm.component.html',
    styleUrls: ['./password-reset-confirm.component.css']
})
export class PasswordResetConfirmComponent implements OnInit {
    newPasswordForm;
    errorMessages: object;
    names = {password: '', repeatedPassword: ''};

    constructor(private fb: FormBuilder, private http: Http, private router: Router, private modalService: MzModalService,
                private route: ActivatedRoute) {
        this.names = {
            password: 'password' + new Date().getTime(),
            repeatedPassword: 'repeatedPassword' + new Date().getTime(),
        };
    }

    checkPassword(form) {
        if (!form._parent) {
            return {passwordToShort: true};
        }
        const controls = form._parent.controls;
        const lengthAccepted = controls.password.value.toString().length >= 8;
        if (lengthAccepted) {
            return true;
        }
        return {passwordToShort: true};
    }

    checkRepeatPassword(form) {
        if (!form._parent) {
            return {isNotEqual: true};
        }
        const controls = form._parent.controls;
        const equal = controls.password.value === controls.repeatedPassword.value;
        if (equal) {
            return true;
        }
        return {isNotEqual: true};
    }

    ngOnInit() {
        this.newPasswordForm = this.fb.group({
            password: ['', this.checkPassword],
            repeatedPassword: ['', this.checkRepeatPassword],
        });

        this.errorMessages = {
            password: {
                passwordToShort: `Wpisane hasło musi mieć minimum 8 znaków`,
            },
            repeatedPassword: {
                isNotEqual: `Wpisane hasło musi się powtarzać`,
            }
        };
    }

    newPassword() {
        if (this.newPasswordForm.status === 'INVALID') {
            return false;
        }
        const data = {
            code: this.route.snapshot.paramMap.get('code'),
            password: sha512(this.newPasswordForm.value.password),
        };
        const resultObservable = this.http.post<Result>('new-password', data);
        resultObservable.subscribe(newPasswordResult => {
            if (newPasswordResult.success) {
                this.router.navigate(['/haslo-zmieniono']);
            } else if (newPasswordResult.error === 'WrondPasswordResetCodeException') {
                this.modalService.open(WrongCodeModalComponent);
            }
        });
        return false;
    }
}
