import {Component, OnInit} from '@angular/core';
import {Http} from '../app.http';
import { Events } from 'ionic-angular';

class Result {
    logged: boolean;
    isAdmin: boolean;
}

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
    user = {logged: false, isAdmin: false}

    constructor(private http: Http, private events: Events) {
        const userComponent = this;
        events.subscribe('user:reload', () => {
            userComponent.reload();
        });
    }

    ngOnInit() {
        this.reload();
    }

    reload() {
        const resultObservable = this.http.get<Result>('user-status');
        resultObservable.subscribe(statusResult => {
            if (statusResult.logged) {
                this.user.logged = statusResult.logged;
                this.user.isAdmin = statusResult.isAdmin;
                this.events.publish('topBar:setUser', this.user);
            }
        });
    }

}
