import {Component, OnInit} from '@angular/core';
import {Http} from '../../app.http';
import {ActivatedRoute} from '@angular/router';

class Result {
    success: boolean;
    error: string;
}

@Component({
    selector: 'app-registration-confirm',
    templateUrl: './registration-confirm.component.html',
    styleUrls: ['./registration-confirm.component.css']
})
export class RegistrationConfirmComponent implements OnInit {
    status: string = null;

    constructor(private http: Http, private route: ActivatedRoute) {
    }

    ngOnInit() {
        const code = this.route.snapshot.paramMap.get('code');
        const resultObservable = this.http.get<Result>('register-confirm/' + code);
        resultObservable.subscribe(registerResult => {
            if (registerResult.success) {
                this.status = 'success';
            } else if (registerResult.error === 'AccountExistsException') {
                this.status = 'account-exists';
            } else {
                this.status = 'error';
            }
        });
    }

}
