import {Component, OnInit} from '@angular/core';

import {FormBuilder, Validators} from '@angular/forms';
import {Http} from '../../app.http';
import {sha512} from 'js-sha512';
import {Router} from '@angular/router';
import {AccountExistsModalComponent} from './account-exists-modal/account-exists-modal.component';
import {MzModalService} from 'ngx-materialize';

class Result {
    success: boolean;
    error: string;
}

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
    registerForm;
    errorMessages: object;
    names = {email: '', password: '', repeatedPassword: ''};

    constructor(private fb: FormBuilder, private http: Http, private router: Router, private modalService: MzModalService) {
        this.names = {
            email: 'nameEmail' + new Date().getTime(),
            password: 'password' + new Date().getTime(),
            repeatedPassword: 'repeatedPassword' + new Date().getTime(),
        };
    }

    checkPassword(form) {
        if (!form._parent) {
            return {passwordToShort: true};
        }
        const controls = form._parent.controls;
        const lengthAccepted = controls.password.value.toString().length >= 8;
        if (lengthAccepted) {
            return true;
        }
        return {passwordToShort: true};
    }

    checkRepeatPassword(form) {
        if (!form._parent) {
            return {isNotEqual: true};
        }
        const controls = form._parent.controls;
        const equal = controls.password.value === controls.repeatedPassword.value;
        if (equal) {
            return true;
        }
        return {isNotEqual: true};
    }

    ngOnInit() {
        this.registerForm = this.fb.group({
            email: ['', [Validators.pattern(/[a-z0-9/.]+@[a-z0-9/.]+.[a-z0-9]+/)]],
            password: ['', this.checkPassword],
            repeatedPassword: ['', this.checkRepeatPassword],
            rulesCheckbox: [false, [Validators.required]],
        });

        this.errorMessages = {
            password: {
                passwordToShort: `Wpisane hasło musi mieć minimum 8 znaków`,
            },
            repeatedPassword: {
                isNotEqual: `Wpisane hasło musi się powtarzać`,
            }
        };
    }

    register() {
        if (this.registerForm.status === 'INVALID') {
            return false;
        }
        const data = {
            mail: this.registerForm.value.email,
            password: sha512(this.registerForm.value.password),
        };
        const resultObservable = this.http.post<Result>('register', data);
        resultObservable.subscribe(registerResult => {
            if (registerResult.success) {
                this.router.navigate(['/zarejestrowano']);
            } else if (registerResult.error === 'AccountExistsException') {
                this.modalService.open(AccountExistsModalComponent);
            }
        });
        return false;
    }
}
