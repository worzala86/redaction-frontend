import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountExistsModalComponent } from './account-exists-modal.component';

describe('AccountExistsModalComponent', () => {
  let component: AccountExistsModalComponent;
  let fixture: ComponentFixture<AccountExistsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountExistsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountExistsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
