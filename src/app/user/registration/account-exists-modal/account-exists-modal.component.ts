import {Component, OnInit} from '@angular/core';
import {MzBaseModal} from 'ngx-materialize';

@Component({
    selector: 'app-account-exists-modal',
    templateUrl: './account-exists-modal.component.html',
    styleUrls: ['./account-exists-modal.component.css']
})
export class AccountExistsModalComponent extends MzBaseModal {
}
