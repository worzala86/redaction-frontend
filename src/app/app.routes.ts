import {RegistrationComponent} from './user/registration/registration.component';
import {RegistrationCompleteComponent} from './user/registration-complete/registration-complete.component';
import {RegistrationConfirmComponent} from './user/registration-confirm/registration-confirm.component';
import {LoginComponent} from './user/login/login.component';
import {PasswordResetComponent} from './user/password-reset/password-reset.component';
import {PasswordResetCompleteComponent} from './user/password-reset-complete/password-reset-complete.component';
import {PasswordResetConfirmComponent} from './user/password-reset-confirm/password-reset-confirm.component';
import {PasswordChangedComponent} from './user/password-changed/password-changed.component';
import {AdminProductsComponent} from './products/admin-products/admin-products.component';
import {AdminProductEditComponent} from './products/admin-product-edit/admin-product-edit.component';

export const Routes = [
    {path: 'rejestracja', component: RegistrationComponent},
    {path: 'zarejestrowano', component: RegistrationCompleteComponent},
    {path: 'aktywacja/:code', component: RegistrationConfirmComponent},
    {path: 'logowanie', component: LoginComponent},
    {path: 'reset-hasla', component: PasswordResetComponent},
    {path: 'zresetowano-haslo', component: PasswordResetCompleteComponent},
    {path: 'nowe-haslo/:code', component: PasswordResetConfirmComponent},
    {path: 'haslo-zmieniono', component: PasswordChangedComponent},
    {path: 'admin/produkty', component: AdminProductsComponent},
    {path: 'admin/produkt/dodaj', component: AdminProductEditComponent},
    {path: 'admin/produkt/:id/edycja', component: AdminProductEditComponent},
]
