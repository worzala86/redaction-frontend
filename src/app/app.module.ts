import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Routes} from './app.routes';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegistrationComponent} from './user/registration/registration.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {
    MzButtonModule, MzInputModule, MzCheckboxModule, MzNavbarModule, MzIconModule, MzIconMdiDirective, MzCardModule,
    MzValidationModule, MzSpinnerModule, MzModalModule, MzSidenavModule, MzCollapsibleModule, MzDropdownModule
} from 'ngx-materialize';
import {RegistrationCompleteComponent} from './user/registration-complete/registration-complete.component';
import {RegistrationConfirmComponent} from './user/registration-confirm/registration-confirm.component';
import {AccountExistsModalComponent} from './user/registration/account-exists-modal/account-exists-modal.component';
import {LoginComponent} from './user/login/login.component';
import {AccountNotFoundModalComponent} from './user/login/account-not-found-modal/account-not-found-modal.component';
import {AccountNotFoundModalComponent as AccountNotFoundInResetPasswordModalComponent} from './user/password-reset/account-not-found-modal/account-not-found-modal.component';
import {IncorrectPasswordModalComponent} from './user/login/incorrect-password-modal/incorrect-password-modal.component';
import {UserComponent} from './user/user.component';
import {Events} from 'ionic-angular';
import {SideNavComponent} from './side-nav/side-nav.component';
import {PasswordResetComponent} from './user/password-reset/password-reset.component';
import { PasswordResetCompleteComponent } from './user/password-reset-complete/password-reset-complete.component';
import { PasswordResetConfirmComponent } from './user/password-reset-confirm/password-reset-confirm.component';
import { WrongCodeModalComponent } from './user/password-reset-confirm/wrong-code-modal/wrong-code-modal.component';
import { PasswordChangedComponent } from './user/password-changed/password-changed.component';
import { AdminProductsComponent } from './products/admin-products/admin-products.component';
import { AdminProductEditComponent } from './products/admin-product-edit/admin-product-edit.component';
import { AdminProductsDeleteModalComponent } from './products/admin-products/admin-products-delete-modal/admin-products-delete-modal.component';

@NgModule({
    declarations: [
        AppComponent,
        TopBarComponent,
        RegistrationComponent,
        MzIconMdiDirective,
        RegistrationCompleteComponent,
        RegistrationConfirmComponent,
        AccountExistsModalComponent,
        LoginComponent,
        AccountNotFoundModalComponent,
        IncorrectPasswordModalComponent,
        UserComponent,
        SideNavComponent,
        PasswordResetComponent,
        AccountNotFoundInResetPasswordModalComponent,
        PasswordResetCompleteComponent,
        PasswordResetConfirmComponent,
        WrongCodeModalComponent,
        PasswordChangedComponent,
        AdminProductsComponent,
        AdminProductEditComponent,
        AdminProductsDeleteModalComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule.forRoot(Routes),
        HttpClientModule,
        BrowserAnimationsModule,
        MzButtonModule,
        MzInputModule,
        MzCheckboxModule,
        MzNavbarModule,
        MzIconModule,
        MzCardModule,
        FormsModule,
        ReactiveFormsModule,
        MzValidationModule,
        MzSpinnerModule,
        MzModalModule,
        MzSidenavModule,
        MzCollapsibleModule,
        MzDropdownModule,
    ],
    providers: [Events],
    bootstrap: [AppComponent],
    entryComponents: [
        AccountExistsModalComponent,
        AccountNotFoundModalComponent,
        IncorrectPasswordModalComponent,
        AccountNotFoundInResetPasswordModalComponent,
        WrongCodeModalComponent,
        AdminProductsDeleteModalComponent,
    ],
})
export class AppModule {
    constructor() {
        const element = document.getElementById('preloader');
        element.remove();
    }
}
