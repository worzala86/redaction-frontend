import {Component, OnInit} from '@angular/core';
import {Events} from 'ionic-angular';
import {Http} from '../app.http';

class Result {
    success: boolean;
}

@Component({
    selector: 'app-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
    user: object = {logged: false, isAdmin: false};

    constructor(private http: Http, private events: Events) {
        const topBarComponent = this;
        events.subscribe('topBar:setUser', (user) => {
            topBarComponent.setUser(user);
        });
    }

    ngOnInit() {
    }

    setUser(user: object) {
        this.user = user;
    }

    logout() {
        const resultObservable = this.http.get<Result>('logout');
        resultObservable.subscribe(logoutResult => {
            if (logoutResult.success) {
                this.setUser({
                    logged: false,
                    isAdmin: false
                });
            }
        });
    }

}
